import express, { Express, Request, Response } from 'express';
import controllerPackingList from '../controller/controllerPackingList';
import controllerLocation from '../controller/controllerLocation';

const router  = express.Router();

/* GET home page. */
/*
router.get('/', function(req, res, next) {
    console.log('komt hier');
    next();
    res.json({ title: 'index'});
});
*/

router.get('/packlist', controllerPackingList.getPackingList);

router.get('/location', controllerLocation);

router.get('/');

export default router;
