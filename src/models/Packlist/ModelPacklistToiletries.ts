import { interfaceToiletries } from "../../interface/interfacePacklistToiletries";
export default class ModelPacklistToiletries {
    toiletries : interfaceToiletries[];

    constructor() {
        this.toiletries = [
            {
                name: 'Toilettas',
                link: 'www.bol.com',
                isSex: 'any', 
            },
            {
                name: 'Deet Anti-muggen',
                link: 'www.bol.com',
                isSex: 'female', 
            },
            {
                name: 'Tandpasta',
                link: 'www.bol.com',
                isSex: 'female', 
            },
            {
                name: 'Tandenborstel',
                link: 'www.bol.com',
                isSex: 'female', 
            },
            {
                name: 'Zonnebrandcreme',
                link: 'www.bol.com',
                isSex: 'female', 
            },
            {
                name: 'make up',
                link: 'www.bol.com',
                isSex: 'female', 
            },
            {
                name: 'Hair gel',
                link: 'www.bol.com',
                isSex: 'male',
            },
            {
                name: 'Hair gel',
                link: 'www.bol.com',
                isSex: 'male',
            },
        ];
    }

    /**
     * 
     * @param {String} sex 
     * @returns 
     */
    async getToiletriesBySex(sex: string | undefined) {
        return new Promise((resolve) => {
            const isMale: Boolean = sex === 'male';
            const isFemale: Boolean = sex === 'female';
            const isAny: Boolean = sex === undefined;
            // console.log('isAny', isAny);
        
            const toiletries = this.toiletries.filter((toiletItems: any) => {
                if(isAny) {
                    console.log('any:', toiletItems);
                    return toiletItems;
                    // @TODO: change sex type any to something else
                } else if (toiletItems.isSex === sex || toiletItems.isSex === 'any') {
                    console.log(sex, 'toiletItems');
                    return toiletItems;
                } 
            });

            resolve(toiletries);
        });
    }


}