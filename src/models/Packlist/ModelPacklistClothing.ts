export default class ModelPacklistClothing {
    clothing: any;
    constructor() {
        this.clothing = [
            {
                name: 'Camera + oplader',
                link: 'www.bol.com',
                isSex: 'any', 
            },
            {
                name: 'E-reader + oplader',
                link: 'www.bol.com',
                isSex: 'any', 
            },
            {
                name: 'Geheugenkaartjes',
                link: 'www.bol.com',
                isSex: 'any', 
            },
            {
                name: 'iPod / MP3 speler',
                link: 'www.bol.com',
                isSex: 'any', 
            },
            {
                name: 'Noise cancelling koptelefoon',
                link: 'www.bol.com',
                isSex: 'any', 
            },
            {
                name: 'Powerbank',
                link: 'www.bol.com',
                isSex: 'any', 
            },
            {
                name: 'Telefoon + oplader',
                link: 'www.bol.com',
                isSex: 'any', 
            },
        ]
    }

    async getClothing() {
        return new Promise((resolve) => {
            resolve(this.clothing);
        })
    }
}