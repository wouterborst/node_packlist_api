export default class ModelPacklistElectronics {
    electronics: any;
    constructor() {
        this.electronics = [
            {
                name: 'Camera + oplader',
                link: 'www.bol.com',
                isSex: 'any', 
            },
            {
                name: 'E-reader + oplader',
                link: 'www.bol.com',
                isSex: 'any', 
            },
            {
                name: 'Geheugenkaartjes',
                link: 'www.bol.com',
                isSex: 'any', 
            },
            {
                name: 'iPod / MP3 speler',
                link: 'www.bol.com',
                isSex: 'any', 
            },
            {
                name: 'Noise cancelling koptelefoon',
                link: 'www.bol.com',
                isSex: 'any', 
            },
            {
                name: 'Powerbank',
                link: 'www.bol.com',
                isSex: 'any', 
            },
            {
                name: 'Telefoon + oplader',
                link: 'www.bol.com',
                isSex: 'any', 
            },
        ]
    }

    async getElectronics() {
        return new Promise((resolve) => {
            resolve(this.electronics);
        })
    }
}