export default class ModelPacklistTravelDocuments {
    travelDocuments: any;
    constructor() {
        this.travelDocuments = [
            {
                name: 'Plane ticket',
                link: 'www.bol.com',
                isSex: 'any', 
            },
            {
                name: 'Travel insurence',
                link: 'www.bol.com',
                isSex: 'any', 
            },
            {
                name: 'Driver licence',
                link: 'www.bol.com',
                isSex: 'any', 
            },
            {
                name: 'Random reader',
                link: 'www.bol.com',
                isSex: 'any', 
            },
            {
                name: 'Pinpas',
                link: 'www.bol.com',
                isSex: 'any', 
            },
            {
                name: 'Credit card',
                link: 'www.bol.com',
                isSex: 'any', 
            },
        ]
    }

    async getTravelDocuments() {
        return new Promise((resolve) => {
            resolve(this.travelDocuments);
        })
    }
}