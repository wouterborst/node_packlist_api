import { interfaceDestination } from "../interface/interfaceDestinations";

export default class ModelDestinations {
    destination: interfaceDestination[];
    
    constructor() {
        this.destination = [
            {
                placeId: 'thailand',
                placeName: 'Thailand',
                cityId: null,
                cityName: null,
                countryId: 'TH',
                countryName: 'Thailand',
                continent: 'asia',
                location: '-8.7467293,115.1666931',
                defaultTravelType: 'backpacking',
                currency: 'Thai Baht',
            },
            {
                placeId: 'vietnam',
                placeName: 'Vietnam',
                cityId: null,
                cityName: null,
                countryId: 'VN',
                countryName: 'Vietnam',
                continent: 'asia',
                location: '-8.7467293,115.1666931',
                defaultTravelType: 'backpacking',
                currency: 'Vietnamese dong',
            },
            {
                placeId: 'sri-lanka',
                placeName: 'Sri Lanka',
                cityId: null,
                cityName: null,
                countryId: 'LK',
                countryName: 'Sri Lanka',
                continent: 'asia',
                location: '-8.7467293,115.1666931',
                defaultTravelType: 'backpacking',
                currency: 'Sri Lankan Rupee',
            },
            {
                placeId: 'indonesia',
                placeName: 'indonesia',
                cityId: null,
                cityName: null,
                countryId: 'ID',
                countryName: 'Indonesia',
                continent: 'asia',
                location: '-8.7467293,115.1666931',
                defaultTravelType: 'backpacking',
                currency: 'Indonesian Rupiah',
            },
            {
                placeId: 'bali',
                placeName: 'Bali',
                cityId: 'IND',
                cityName: 'Denpasar',
                countryId: 'ID',
                countryName: 'Indonesia',
                continent: 'asia',
                location: '-8.7467293,115.1666931',
                defaultTravelType: 'backpacking',
                currency: 'Indonesian Rupiah',
            },
            {
                placeId: 'berlin',
                placeName: 'berlin',
                cityId: 'BER',
                cityName: 'Berlijn',
                countryId: 'DE',
                countryName: 'Germany',
                continent: 'europa',
                location: '-8.7467293,115.1666931',
                defaultTravelType: 'citytrip',
                currency: 'euro',
            },
            {
                placeId: 'paris',
                placeName: 'Paris',
                cityId: 'PAR',
                cityName: 'Paris',
                countryId: 'FR',
                countryName: 'France',
                continent: 'europa',
                location: '-8.7467293,115.1666931',
                defaultTravelType: 'citytrip',
                currency: 'euro',
            },
            {
                placeId: 'london',
                placeName: 'Londen',
                cityId: 'LDN',
                cityName: 'London',
                countryId: 'UK',
                countryName: 'England',
                continent: 'europa',
                location: '-8.7467293,115.1666931',
                defaultTravelType: 'citytrip',
                currency: 'euro',
            },
            {
                placeId: 'barcelona',
                placeName: 'Barcelona',
                cityId: 'BCN',
                cityName: 'Barcelona',
                countryId: 'SP',
                countryName: 'Spain',
                continent: 'europa',
                location: '-8.7467293,115.1666931',
                defaultTravelType: 'citytrip',
                currency: 'euro',
            },
            {
                placeId: 'lissabon',
                placeName: 'Lissabon',
                cityId: 'LBN',
                cityName: 'Lissabon',
                countryId: 'PO',
                countryName: 'Portugal',
                continent: 'europa',
                location: '-8.7467293,115.1666931',
                defaultTravelType: 'citytrip',
                currency: 'euro',
            },
            {
                placeId: 'copenhagen',
                placeName: 'copenhagen',
                cityId: 'CPH',
                cityName: 'copenhagen',
                countryId: 'DN',
                countryName: 'Denmark',
                continent: 'europa',
                location: '-8.7467293,115.1666931',
                defaultTravelType: 'citytrip',
                currency: 'euro',
            }
        ];
    }

    getLocations() {
        return this.destination;
    }

    getDestanationByDestanationName(destanation: string | undefined) {
        return new Promise((resolve, reject) => {
            if(destanation === undefined) {
                return reject('No matching location found');
            }

            let paramDestanation: string = destanation.toLocaleLowerCase();

            const destanationFilterResult = this.destination.filter((loc: any) => loc.placeId.toLocaleLowerCase() === paramDestanation);
            const hasNoDestanationResult = destanationFilterResult.length === 0;
            
            if(hasNoDestanationResult) {
                // @todo add translation strings
                reject('No matching location found');
                // throw new Error(noLocation.error);
            }

            console.log('locationFilterResult', destanationFilterResult);

            resolve(destanationFilterResult[0])
        })
    }
}
