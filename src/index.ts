import express, { Express, Request, Response } from 'express';

const app : Express = express();
import routes from './routes/router'; 

const port = 3000;

app.use('/', routes);

app.listen(port, () => {
    console.log(`Listening on port: ${port}`);
});