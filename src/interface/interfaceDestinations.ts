import { typeDefaultTravelType } from "../types/globalTypes";
export interface interfaceDestination {
    placeId: string;
    placeName: string;
    cityId: string | null;
    cityName: string | null;
    countryId: string;
    countryName: string;
    continent: string;
    location: string;
    defaultTravelType: typeDefaultTravelType;
    currency: string;
}