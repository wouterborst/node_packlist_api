import express, { Request, Response, NextFunction } from 'express';
import ModelDestinations from '../models/ModelDestinations';

const getLocation = async (req: Request, res: Response, next: NextFunction) => {
    const destinations = new ModelDestinations;
    
    res.json(destinations.getLocations());
};

export default getLocation;
