import express, { Express, Request, Response, NextFunction, response } from 'express';
import { InterfaceQueryPacklist } from '../interface/interfaceQuery';

import ModelDestinations from '../models/ModelDestinations';
import ModelPacklistClothing from '../models/Packlist/ModelPacklistClothing';
import ModelPacklistElectronics from '../models/Packlist/ModelPacklistElectronics';
import ModelPacklistToiletries from '../models/Packlist/ModelPacklistToiletries';
import ModelPacklistTravelDocuments from '../models/Packlist/ModelPacklistTravelDocuments';

export default class controllerPackingList {
    static #packlistGetDestination = async(destanation : string | undefined) => {
        const destinations = new ModelDestinations;   
        return await destinations.getDestanationByDestanationName(destanation);
    }
    
    static #packlistComposeResponse = async(desitnation : string | undefined, sex: string | undefined) => {
        const desitnationData = await this.#packlistGetDestination(desitnation);
        console.log('packlistComposeResponse', desitnationData);
        console.log('sex', sex);
    
        const travelDocuments = new ModelPacklistTravelDocuments;
        const toiletries = new ModelPacklistToiletries;
        const electronics = new ModelPacklistElectronics;
        const clothing = new ModelPacklistClothing;
    
        return Promise.all([
            travelDocuments.getTravelDocuments(),
            toiletries.getToiletriesBySex(sex),
            electronics.getElectronics(),
            clothing.getClothing()
        ]
        ).then((response) => {
            const packlistDocuments = response[0]
            const packlistToiletries = response[1];
            const packlistElectronics = response[2];
            const packlistClothing = response[3]
    
            return {
                destination: desitnationData,
                packlist: {
                    documents: packlistDocuments,
                    toiletries: packlistToiletries,
                    electronics: packlistElectronics,
                    clothing: packlistClothing,
                }
            };      
        }).catch(error => {
            return error;
        });
    };
    
    
    static getPackingList = async (req: Request, res: Response, next: NextFunction) => {
        const { destanation, sex } = req.query as unknown as InterfaceQueryPacklist;
        const hasSex: boolean = sex !== undefined;
    
        return await this.#packlistComposeResponse(destanation, sex)
        .then(response => {    
            res.json({
                status: 200,
                data: response,
            });
        })
        .catch(error => {
            console.log('error', error);
            res.status(404);//Set status to 404 as movie was not found
            res.json({
                status: 404,
                // TODO: add better error message here
                message: 'here error message' 
            });
        });
    };   
}